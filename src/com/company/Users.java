package com.company;

import java.io.Serializable;
import java.util.ArrayList;

public class Users implements Serializable {
    private ArrayList<User> usersList = new ArrayList<>();


    public boolean add(User user) {
        return usersList.add(user);
    }

    public int index_of(User user) {
        return usersList.indexOf(user);
    }

    public boolean username_taken(String username) {
        for (int i = 0; i < usersList.size(); i++) {
            if (username.equals(usersList.get(i).getUsername())) {
                return true;
            }
        }
        return false;
    }

    public User find_user_by_id(String username) {
        for (int i = 0; i < usersList.size(); i++) {
            if (username.equals(usersList.get(i).getUsername())) {
                return usersList.get(i);
            }
        }
        return null;
    }

    public boolean contains(User user) {
        return usersList.contains(user);
    }

    public boolean remove(User user) {
        return usersList.remove(user);
    }

}
