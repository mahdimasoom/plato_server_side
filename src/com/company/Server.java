package com.company;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Objects;

public class Server {
    public static int numberOfuserFiles = 0;
    static Users users = new Users();
    static String userPath = "C:\\Users\\MHA1380\\IdeaProjects\\plato_server_side\\src\\com\\company\\userpath";
    static ArrayList<Game> games;
    static ArrayList<String> games_name;

    public static void main(String[] args) throws IOException {

        try {
            loadData();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        ServerSocket serverSocket = new ServerSocket(1380);
        Socket clientsocket;
        while (true) {
            clientsocket = serverSocket.accept();
            System.out.println("client connected");
            DataInputStream serverdatainputstream = new DataInputStream(clientsocket.getInputStream());
            DataOutputStream serverdataoutputstream = new DataOutputStream(clientsocket.getOutputStream());
            Thread thread = new Clienthandler(clientsocket, serverdatainputstream, serverdataoutputstream);
            thread.start();
        }

    }

    private static void loadData() throws IOException, ClassNotFoundException {
        int numberOfusers = Objects.requireNonNull(new File(Server.userPath).listFiles()).length;
        System.out.println(numberOfusers);

        for (int i = 1; i <= numberOfusers; i++) {
            File file = new File(userPath +"\\"+ i + ".txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            User user = (User) objectInputStream.readObject();
            users.add(user);
            System.out.println("Users: " + user.getUsername());
        }

    }
}
