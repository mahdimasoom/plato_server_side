package com.company;

import java.io.*;
import java.net.Socket;
import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

public class Clienthandler extends Thread {
    private Socket socket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    SimpleDateFormat formatter = new SimpleDateFormat("HH:MM", Locale.US);
    String client_message;
    String tag;
    String server_message;
    User client;
    boolean forced_log_out = false;

    Clienthandler(Socket socket, DataInputStream dataInputStream, DataOutputStream dataOutputStream) {
        this.socket = socket;
        this.dataInputStream = dataInputStream;
        this.dataOutputStream = dataOutputStream;
    }

    @Override
    public void run() {
        while (!forced_log_out) {
            try {
                client_message = dataInputStream.readUTF();
            } catch (IOException e) {
                System.out.println(1);
                logout();
            }
            System.out.println("(android user says " + client_message);
            tag = client_message.substring(0, client_message.indexOf("$"));
            switch (tag) {
                case "signup":
                    try {
                        signup(client_message);
                    } catch (IOException e) {
                        System.out.println(2);
                        logout();
                    }
                    break;
                case "signin":
                    try {
                        signin(client_message);
                    } catch (IOException e) {
                        System.out.println(3);
                        logout();
                    }
                    break;
                case "editprofile":
                    try {
                        edit_profile(client_message);
                    } catch (Exception e) {
                        System.out.println(4);
                        logout();
                    }
                    break;
                case "profile":
                    try {
                        profile();
                    } catch (Exception e) {
                        System.out.println(5);
                        logout();
                    }
                    break;
                case "fragment":
                    try {
                        goto_page(client_message);
                    } catch (Exception e) {
                        System.out.println(6);
                        logout();
                    }
                    break;
                case "addfriend":
                    try {
                        add_friend(client_message);
                    } catch (Exception e) {
                        System.out.println(7);
                        logout();
                    }
                case "sendmessage":
                    try {
                        send_message(client_message);
                    } catch (Exception e) {
                        logout();
                    }
                case "getchat":
                    try {
                        get_chat_with(client_message);
                    } catch (Exception e) {
                        System.out.println(8);
                        logout();
                    }
                    break;
                case "logout":
                    logout();
                    break;
            }
        }

    }

    private void add_friend(String client_message) throws IOException {
        String username = client_message.substring(client_message.indexOf("$") + 1);
        String username_client = client.getUsername();
        User user = Server.users.find_user_by_id(username);

        if (user != null) {
            if (!username.equals(username_client)) {
                if (!client.getFriends().contains(username)) {
                    user.getFriends().add(username_client);
                    client.getFriends().add(username);
                    server_message = "user " + username + " added to your friends";
                } else {
                    server_message = "this user already exists in your friend list";
                }
            } else {
                server_message = "you can not add your self to your friend list";
            }
        } else {
            server_message = "this user does not exist";
        }
        dataOutputStream.writeUTF(server_message);
    }

    private void get_chat_with(String client_message) throws IOException {
        String username = client_message.substring(client_message.indexOf("$") + 1);
        User user = Server.users.find_user_by_id(username);
        if (user != null) {
            ArrayList<Message> chat = new ArrayList<>();
            int size1 = client.getMessages_sent().size(), size2 = client.getMessages_recieved().size();
            for (int i = 0; i < size1; i++) {
                if (client.getMessages_sent().get(i).getReciever().equals(username)) {
                    chat.add(client.getMessages_sent().get(i));
                }
            }
            for (int i = 0; i < size2; i++) {
                if (client.getMessages_recieved().get(i).getSender().equals(username)) {
                    chat.add(client.getMessages_recieved().get(i));
                }
            }
            User.sort_messages(chat);
            server_message = make_string_out_of_chat(chat);
        } else {
            server_message = "this user does not exist";
        }
        dataOutputStream.writeUTF(server_message);
    }

    private String make_string_out_of_chat(ArrayList<Message> chat) {
        StringBuilder server_message = new StringBuilder("chat$");
        Integer size = chat.size();
        if (size > 0) {
            server_message.append(size.toString()).append(" ");
            Message message;
            for (int i = 0; i < size; i++) {
                message = chat.get(i);
                server_message.append(message.getSender()).append(" ").append(message.getReciever()).append(" ").append(formatter.format(message.getDate())).append(" ").append(message.getBody()).append(" ");
            }
            return server_message.toString();
        }
        return "empty$";
    }

    private void goto_page(String client_message) throws IOException {
        String page = client_message.substring(client_message.indexOf("$") + 1);
        ArrayList<String> friends;
        ArrayList<String> chats;
        ArrayList<String> games;
        switch (page) {
            case "friend":

                friends = client.getFriends();
                server_message = "friends$";
                for (String friend : friends) {
                    server_message = server_message + friend + " ";
                }
                System.out.println(server_message);
                break;
            case "game":
                games = Server.games_name;
                server_message = "game$";
                for (int i = 0; i < games.size(); i++) {
                    server_message = server_message + games.get(i) + " ";
                }
                break;
            case "chat":
                chats = client.get_chats_fragment_user();
                server_message = "chats$";
                for (int i = 0; i < chats.size(); i++) {
                    server_message = server_message + chats.get(i) + " ";
                }
                break;
        }
        dataOutputStream.writeUTF(server_message);
    }


    private void profile() throws IOException {
        server_message = "profile$" + client.getUsername() + " " + client.getBio();
        dataOutputStream.writeUTF(server_message);
    }


    private void signin(String client_message) throws IOException {
        String username = client_message.substring(client_message.indexOf("$") + 1, client_message.indexOf(" "));
        String password = client_message.substring(client_message.indexOf(" ") + 1);
        if (Server.users.find_user_by_id(username) != null) {
            if (!Server.users.find_user_by_id(username).isLogged_in()) {
                Server.users.find_user_by_id(username).setLogged_in(false);
            }
            User temp = Server.users.find_user_by_id(username);
            if (temp != null) {
                if (temp.getPassword().equals(password)) {
                    server_message = "success$loggedin";
                    System.out.println("server says " + temp.toString() + "logged in successfully");
                    client = temp;
                    temp.setLogged_in(true);
                } else {
                    server_message = "error$password was wrong";
                    System.out.println("server says wrong password was inserted .");
                }
            } else {
                server_message = "error$user with this username does not exist";
                System.out.println("server says something went wrong in logging in .");
            }

        } else {
            server_message = "error$this username does not exist";
        }
        this.dataOutputStream.writeUTF(server_message);
        this.dataOutputStream.flush();

    }

    private void signup(String client_message) throws IOException {
        String username = client_message.substring(client_message.indexOf("$") + 1, client_message.indexOf(" "));
        String password = client_message.substring(client_message.indexOf(" ") + 1);

        if (username.length() == 0) {
            server_message = "error$your username's size must be more than 0";
            if (password.length() <= 5) {
                server_message = server_message + " & your password size must be more than 5";
            }
            System.out.println("server says something went wrong in signing up .");

        } else if (Server.users.username_taken(username)) {
            server_message = "error$";
            server_message = server_message + "this user name has already been taken";
            if (password.length() <= 5) {
                server_message = server_message + " & your password size must be more than 5";
            }
            System.out.println("server says something went wrong in signing up .");
        } else if (password.length() <= 5) {
            server_message = "error$";
            server_message = server_message + " & your password size must be more than 5";
            System.out.println("server says something went wrong in signing up .");
        } else {
            User temp = new User(username, password);
            Server.users.add(temp);
            userToFile(temp);
            server_message = "success$signedup";
            System.out.println("server says" + temp.toString() + "signedup .");
            temp.setLogged_in(true);
            client = temp;
        }


        this.dataOutputStream.writeUTF(server_message);
        this.dataOutputStream.flush();

    }

    public void edit_profile(String client_message) throws IOException, ClassNotFoundException {

        String tag_being_edited = client_message.substring(client_message.indexOf("$") + 1, client_message.indexOf(" "));
        String being_edited_to = client_message.substring(client_message.indexOf(" ") + 1);
        boolean success;
        switch (tag_being_edited) {
            case "username":
                success = set_new_username(being_edited_to);
                if (success) {
                    System.out.println("Android client " + client.getUsername() + " username updated .");
                    server_message = "username successfully updated !";
                    updateuserfile(client);
                } else {
                    server_message = "username is not valid !(try another one)";
                }

                break;
            case "password":
                success = set_new_password(being_edited_to);
                if (success) {
                    System.out.println("Android client" + client.getUsername() + " password updated .");
                    server_message = "password successfully updated !";
                    updateuserfile(client);
                } else {
                    server_message = "password size must be more than 5";
                }

                break;
            case "bio":
                client.setBio(being_edited_to);
                User user = Server.users.find_user_by_id(client.getUsername());
                user.setBio(being_edited_to);
                System.out.println("Android client" + client.getUsername() + " bio updated .");
                server_message = "bio successfully updated !";
                updateuserfile(client);

                break;
            default:
                System.out.println("server says something went wrong in editing profile .");
                server_message = "error$updatingprofile";

        }
        dataOutputStream.writeUTF(server_message);
        dataOutputStream.flush();
    }

    public static File getClient_file_name(User user) throws IOException, ClassNotFoundException {
        if (user != null) {
            int numberOfusers = Objects.requireNonNull(new File(Server.userPath + "\\").listFiles()).length;


            for (int i = 1; i <= numberOfusers; i++) {
                File file = new File(Server.userPath + "\\" + i + ".txt");
                FileInputStream fileInputStream = new FileInputStream(file);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                User temp = (User) objectInputStream.readObject();
                if (temp.getUsername().equals(user.getUsername())) {
                    return file.getAbsoluteFile();
                }
            }
            return null;
        }
        return null;
    }

    private void logout() {
        try {
            updateuserfile(client);

            System.out.println("server says " + client.toString() + "logged out");
            dataInputStream.close();
            dataOutputStream.close();
            socket.close();
            forced_log_out = true;
            client.setLogged_in(false);
        } catch (Exception e) {
            System.out.println();
        } finally {

            try {
                dataInputStream.close();
                dataOutputStream.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public static void userToFile(Object object) {
        try {
            File file = new File(Server.userPath + "\\" + (Server.numberOfuserFiles + 1) + ".txt");
            if (!file.exists()) {
                file.createNewFile();
                Server.numberOfuserFiles++;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(object);
            objectOutputStream.flush();
            System.out.println("user successfully saved !!!!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void updateuserfile(User user) throws IOException {
        int position = Server.users.index_of(user);
        File file = new File(Server.userPath + "\\" + (position + 1) + ".txt");
        if (file.exists()) {
            file.delete();
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(user);
            objectOutputStream.flush();
            System.out.println("user successfully updated !!!!");
        } else {
            userToFile(user);
        }

    }

    public boolean set_new_username(String username) {
        if (username.length() == 0) {
            return false;
        }
        if (Server.users.username_taken(username)) {
            return false;
        }
        client.setUsername(username);
        return true;
    }

    public boolean set_new_password(String password) {
        if (password.length() >= 5) {
            client.setPassword(password);
            return true;
        } else {
            return false;
        }
    }

    public void send_message(String client_message) {
         String usernamereciever, body;
        String[] message_spliter = client_message.split(" ");
        usernamereciever = message_spliter[1];
        body = message_spliter[2];
        Message message = new Message(client.getUsername(), usernamereciever, body);
        message.add_to_sender_reciever();

    }

}
