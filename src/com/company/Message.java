package com.company;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
    private Date date;
    private String sender;
    private String reciever;
    private String body;

    public Message(String sender, String reciever, String body) {
        this.date = new Date();
        this.sender = sender;
        this.reciever = reciever;
        this.body = body;
    }

    public Message(Date date, String sender, String reciever, String body) {
        this.date = date;
        this.sender = sender;
        this.reciever = reciever;
        this.body = body;
    }

    public void add_to_sender_reciever() {
        User sender = Server.users.find_user_by_id(this.sender);
        User reciever = Server.users.find_user_by_id(this.reciever);
        sender.getMessages_sent().add(this);
        reciever.getMessages_recieved().add(this);
    }

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

