package com.company;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class User implements Serializable {
    private String username;
    private String password;
    private String bio = "Hey i am using plato come play with me .";
    private HashMap<String, Integer> points;
    private ArrayList<String> friends;
    private ArrayList<Message> messages_recieved;
    private ArrayList<Message> messages_sent;
    private boolean logged_in = false;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.friends = new ArrayList<>();
        this.messages_recieved = new ArrayList<>();
        this.messages_sent = new ArrayList<>();

    }

    public ArrayList<String> get_chats_fragment_user() {
        ArrayList<String> names = new ArrayList<>();
        String name;
        for (int i = 0; i < messages_sent.size(); i++) {
            name = messages_sent.get(i).getReciever();
            if(!names.contains(name)){
                names.add(name);
            }
        }
        for (int i = 0; i < messages_recieved.size(); i++) {
            name = messages_recieved.get(i).getSender();
            if(!names.contains(name)){
                names.add(name);
            }
        }
        return names;
    }

    public static  void sort_messages(ArrayList<Message> chat) {
        int n = chat.size();
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (chat.get(j).getDate().compareTo(chat.get(j + 1).getDate()) > 0) {
                    Message temp = chat.get(j);
                    chat.set(j, chat.get(j + 1));
                    chat.set(j + 1, temp);
                }
    }

    public String getBio() {
        return bio;
    }

    public HashMap<String, Integer> getPoints() {
        return points;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public void setPoints(HashMap<String, Integer> points) {
        this.points = points;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public boolean isLogged_in() {
        return logged_in;
    }

    public void setLogged_in(boolean logged_in) {
        this.logged_in = logged_in;
    }

    public ArrayList<String> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<String> friends) {
        this.friends = friends;
    }

    public ArrayList<Message> getMessages_recieved() {
        return messages_recieved;
    }

    public void setMessages_recieved(ArrayList<Message> messages_recieved) {
        this.messages_recieved = messages_recieved;
    }

    public ArrayList<Message> getMessages_sent() {
        return messages_sent;
    }

    public void setMessages_sent(ArrayList<Message> messages_sent) {
        this.messages_sent = messages_sent;
    }
}
